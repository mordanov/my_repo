from app import app
from flask import request
from flask import render_template
from app import func_search as fs
from app import templates

@app.route('/', methods=['GET', 'POST'])
def index():
    if(request.method == 'POST'):
        entries = fs.complain_search(request.form['searchquery'])
        return render_template('page_template.html', reccount=len(entries), entries=entries,
                               searchtext=request.form['searchquery'])
    else:
        return render_template('page_template.html', reccount=0, entries=[], searchtext="")
