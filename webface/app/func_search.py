from rutermextract import TermExtractor
import pyodbc

def complain_search(search_query):
    connstring = 'DRIVER={SQL Server};SERVER=CSKO-W-CS-22285;DATABASE=dbcomplains;UID=complainer;PWD=complainer'
    conn = pyodbc.connect(connstring)
    crsr = conn.cursor()

    sql = 'DECLARE	@return_value int; EXEC	@return_value = dbo.up_NewQuery @Query=?; SELECT \'Return Value\' = @return_value;'
    args = [search_query]
    crsr.execute(sql, args)
    queryid = crsr.fetchone()[0]

    term_ex = TermExtractor()
    for term in term_ex(search_query):
        sql = 'EXEC dbo.up_NewQueryTerm @Term=?, @QueryID=?;'
        args = [str(term), queryid]
        crsr.execute(sql, args)
    conn.commit()

    sql = 'EXEC dbo.up_CountTFIDF_Query @QueryID = ?'
    args = [queryid]
    crsr.execute(sql, args)
    conn.commit()

    sql = 'EXEC dbo.up_Select4Query @QueryID = ?'
    crsr.execute(sql, args)
    resultset = crsr.fetchall()

    crsr.close()

    return resultset
