USE [master]
GO
/****** Object:  Database [dbcomplains]    Script Date: 02.08.2017 13:20:20 ******/
CREATE DATABASE [dbcomplains]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'complains_Data', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL12.MSSQLSERVER\MSSQL\DATA\complains_Data.mdf' , SIZE = 61696KB , MAXSIZE = UNLIMITED, FILEGROWTH = 10%)
 LOG ON 
( NAME = N'complains_Log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL12.MSSQLSERVER\MSSQL\DATA\complains_Log.ldf' , SIZE = 155648KB , MAXSIZE = 2048GB , FILEGROWTH = 1024KB )
GO
ALTER DATABASE [dbcomplains] SET COMPATIBILITY_LEVEL = 120
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [dbcomplains].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [dbcomplains] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [dbcomplains] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [dbcomplains] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [dbcomplains] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [dbcomplains] SET ARITHABORT OFF 
GO
ALTER DATABASE [dbcomplains] SET AUTO_CLOSE ON 
GO
ALTER DATABASE [dbcomplains] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [dbcomplains] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [dbcomplains] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [dbcomplains] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [dbcomplains] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [dbcomplains] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [dbcomplains] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [dbcomplains] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [dbcomplains] SET  ENABLE_BROKER 
GO
ALTER DATABASE [dbcomplains] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [dbcomplains] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [dbcomplains] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [dbcomplains] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [dbcomplains] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [dbcomplains] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [dbcomplains] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [dbcomplains] SET RECOVERY SIMPLE 
GO
ALTER DATABASE [dbcomplains] SET  MULTI_USER 
GO
ALTER DATABASE [dbcomplains] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [dbcomplains] SET DB_CHAINING OFF 
GO
ALTER DATABASE [dbcomplains] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [dbcomplains] SET TARGET_RECOVERY_TIME = 60 SECONDS 
GO
ALTER DATABASE [dbcomplains] SET DELAYED_DURABILITY = DISABLED 
GO
USE [dbcomplains]
GO
/****** Object:  User [complainer]    Script Date: 02.08.2017 13:20:20 ******/
CREATE USER [complainer] FOR LOGIN [complainer] WITH DEFAULT_SCHEMA=[dbo]
GO
ALTER ROLE [db_owner] ADD MEMBER [complainer]
GO
/****** Object:  UserDefinedFunction [dbo].[sf_GetIntersectonCost]    Script Date: 02.08.2017 13:20:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date, ,>
-- Description:	<Description, ,>
-- =============================================
CREATE FUNCTION [dbo].[sf_GetIntersectonCost]
(
	-- Add the parameters for the function here
	@QueryID int,
	@ComplainID int
)
RETURNS float
AS
BEGIN
	-- Declare the return variable here
	DECLARE @cost as float

	-- Add the T-SQL statements to compute the return value here
	select @cost = sqrt(sum(power(q.tf*q.idf, 2))) from terms t
	inner join terms_norm tn on tn.id = t.id_term
	inner join qterms q on q.qterm = tn.term
	where t.id_complain = @ComplainID and q.id_query = @QueryID
 
 	-- Return the result of the function
	RETURN @cost

END


GO
/****** Object:  Table [dbo].[complains]    Script Date: 02.08.2017 13:20:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[complains](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[info] [nvarchar](max) NULL,
	[bank] [nvarchar](max) NULL,
	[client] [nvarchar](max) NULL,
	[clientoffice] [nvarchar](max) NULL,
	[template] [nvarchar](max) NULL,
	[protocol] [nvarchar](max) NULL,
	[decision] [nvarchar](max) NULL,
	[subject] [nvarchar](max) NULL,
	[cost] [float] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[qterms]    Script Date: 02.08.2017 13:20:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[qterms](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[id_query] [int] NULL,
	[qterm] [nvarchar](max) NULL,
	[tf] [float] NULL,
	[idf] [float] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[queries]    Script Date: 02.08.2017 13:20:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[queries](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[qtext] [nvarchar](max) NULL,
	[cost] [float] NULL,
	[qdate] [datetime] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[terms]    Script Date: 02.08.2017 13:20:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[terms](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[tcount] [int] NULL,
	[id_complain] [int] NULL,
	[id_term] [int] NULL,
	[tf] [float] NULL,
	[idf] [float] NULL
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[terms_norm]    Script Date: 02.08.2017 13:20:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[terms_norm](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[term] [nvarchar](max) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
ALTER TABLE [dbo].[queries] ADD  CONSTRAINT [DF_queries_qdate]  DEFAULT (getdate()) FOR [qdate]
GO
/****** Object:  StoredProcedure [dbo].[up_CountTFIDF_Query]    Script Date: 02.08.2017 13:20:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[up_CountTFIDF_Query]
	-- Add the parameters for the stored procedure here
	@QueryID int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
declare @doccount as float
declare @qcount as float
declare @cost as float

SELECT @doccount = COUNT(*) FROM complains
SELECT @qcount = COUNT(*) FROM qterms WHERE id_query = @QueryID

UPDATE qterms SET tf = 1.0/@qcount, idf = log(@doccount/qtrm.idf)
FROM qterms q
INNER JOIN 
(SELECT qt.id, SUM(t.tcount) + 1 as idf FROM terms t
INNER JOIN terms_norm tn ON t.id_term = tn.id
INNER JOIN qterms qt ON qt.qterm = tn.term
WHERE qt.id_query = @QueryID
GROUP BY qt.id) as qtrm ON qtrm.id = q.id
WHERE id_query = @QueryID

SELECT @cost = sqrt(sum(power(qt.tf*qt.idf, 2))) FROM queries q
INNER JOIN qterms qt ON qt.id_query = q.id
WHERE q.id = @QueryID

UPDATE queries SET cost = @cost
WHERE id = @QueryID

END


GO
/****** Object:  StoredProcedure [dbo].[up_InsertComplain]    Script Date: 02.08.2017 13:20:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[up_InsertComplain]
	-- Add the parameters for the stored procedure here
	@info varchar(max),
	@bank varchar(max),
	@client varchar(max),
	@clientoffice varchar(max),
	@template varchar(max),
	@protocol varchar(max),
	@decision varchar(max),
	@subject varchar(max)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	INSERT INTO complains (info, bank, client, clientoffice, template, protocol, decision, subject) values 
		(@info, @bank, @client, @clientoffice, @template, @protocol, @decision, @subject)
END

GO
/****** Object:  StoredProcedure [dbo].[up_NewQuery]    Script Date: 02.08.2017 13:20:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[up_NewQuery] 
	-- Add the parameters for the stored procedure here
	@Query nvarchar(MAX)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	INSERT INTO dbo.queries (qtext) VALUES (@Query)

    -- Insert statements for procedure here
	RETURN @@IDENTITY
END


GO
/****** Object:  StoredProcedure [dbo].[up_NewQueryTerm]    Script Date: 02.08.2017 13:20:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[up_NewQueryTerm] 
	-- Add the parameters for the stored procedure here
	@Term nvarchar(MAX), @QueryID int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	INSERT INTO dbo.qterms (qterm, id_query) VALUES (@Term, @QueryID)

END


GO
/****** Object:  StoredProcedure [dbo].[up_NewTerm]    Script Date: 02.08.2017 13:20:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[up_NewTerm]
	-- Add the parameters for the stored procedure here
	@term varchar(max),
	@tcount int,
	@id_complain int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	declare @tid as int

	select @tid = id from terms_norm where term = @term
	if @tid is null
	  begin
	    insert into terms_norm (term) values (@term)
	    set @tid = @@IDENTITY;
	  end

	insert into terms (tcount, id_complain, id_term) values (@tcount, @id_complain, @tid)
END

GO
/****** Object:  StoredProcedure [dbo].[up_Select4Query]    Script Date: 02.08.2017 13:20:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[up_Select4Query]
	-- Add the parameters for the stored procedure here
	@QueryID as int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

declare @cost as float

select @cost = cost from queries where id = @QueryID

select distinct top 100 c.id, c.info, c.decision, c.subject, c.protocol, round(icost.intersection_cost/(@cost*c.cost), 2) as coeff from complains c
inner join terms t ON c.id = t.id_complain
inner join terms_norm tn on tn.id = t.id_term
inner join qterms q on q.qterm = tn.term
inner join
(select t.id_complain, sqrt(sum(power(q.tf*q.idf, 2))) as intersection_cost from terms t
	inner join terms_norm tn on tn.id = t.id_term
	inner join qterms q on q.qterm = tn.term
	where q.id_query = @QueryID
	group by t.id_complain) as icost
on icost.id_complain = c.id
where q.id_query = @QueryID
order by coeff desc

END


GO
/****** Object:  StoredProcedure [dbo].[up_Update_Costs]    Script Date: 02.08.2017 13:20:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[up_Update_Costs]
	-- Add the parameters for the stored procedure here
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

UPDATE complains SET cost = trm.cost
FROM complains c
INNER JOIN
(SELECT id_complain, sqrt(sum(power(tf*idf, 2))) as cost from terms group by id_complain) as trm
ON c.id = trm.id_complain

RETURN @@ROWCOUNT
END


GO
/****** Object:  StoredProcedure [dbo].[up_Update_TFIDF]    Script Date: 02.08.2017 13:20:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[up_Update_TFIDF]
	-- Add the parameters for the stored procedure here
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

declare @doccount as int

SELECT @doccount = COUNT(*) FROM complains

UPDATE terms SET tf = tfidf.tf, idf = tfidf.idf
FROM 
(SELECT t.id, t.id_term, (t.tcount/g.cterm) as tf, g.id_complain, log(CAST(@doccount as float) / m.ctcount) AS idf
FROM terms AS t INNER JOIN 
(SELECT t1.id_complain, CAST(COUNT(t1.id_term) AS float) AS cterm
FROM terms AS t1
GROUP BY t1.id_complain)
AS g ON t.id_complain = g.id_complain INNER JOIN 
(SELECT t2.id_term, CAST(SUM(t2.tcount) AS float) AS ctcount
FROM terms AS t2
WHERE t2.id_term IS NOT NULL
GROUP BY t2.id_term)
AS m ON m.id_term = t.id_term) as tfidf
WHERE terms.id = tfidf.id

RETURN @@ROWCOUNT

END


GO
USE [master]
GO
ALTER DATABASE [dbcomplains] SET  READ_WRITE 
GO
