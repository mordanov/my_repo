# жалобы
import pandas as pd
import pyodbc

cutstr1="Подробное описание от инициатора"
cutstr2="Нестандартный ЗНО"

def left(s, amount):
    return s[:amount]

def right(s, amount):
    return s[-amount:]

def mid(s, offset, amount):
    return s[offset:offset+amount]

connstring = 'DRIVER={SQL Server};SERVER=CSKO-W-CS-22285;DATABASE=dbcomplains;UID=complainer;PWD=complainer'
conn = pyodbc.connect(connstring)

df = pd.read_excel('D:\Python\Complains\Жалобы_сентябрь.xlsx', 'Sheet1')

for index, row in df.iterrows():
   bank = str(row[0]).replace('nan', '').replace('\'', '')
   client = str(row[5]).replace('nan', '').replace('\'', '')
   clientoffice = str(row[7]).replace('nan', '').replace('\'', '')
   template = str(row[14]).replace('nan', '').replace('\'', '')
   protocol = str(row[18]).replace('nan', '').replace('\'', '')
   decision = str(row[19]).replace('nan', '').replace('\'', '')
   subject = str(row[41]).replace('nan', '').replace('\'', '')
   info = str(row[17]).replace('nan', '').replace('\'', '')

   info = info.replace(cutstr2, "").replace(cutstr1, "")

   cur = conn.cursor()
   sql = "EXEC dbo.up_InsertComplain @info = ?, @bank = ?, @client = ?, @clientoffice = ?, @template = ?, @protocol = ?," \
         " @decision = ?, @subject = ?;"
   args = [info, bank, client, clientoffice, template, protocol, decision, subject]
   cur.execute(sql, args)

conn.commit()
cur.close()
conn.close()
