import pyodbc
from rutermextract import TermExtractor

term_ex = TermExtractor()

connstring = 'DRIVER={SQL Server};SERVER=CSKO-W-CS-22285;DATABASE=dbcomplains;UID=complainer;PWD=complainer'
conn = pyodbc.connect(connstring)

cur=conn.cursor()
sql = 'select id, info, protocol as fullinfo from complains'
cur.execute(sql)
cmpls = cur.fetchall()
cur1 = conn.cursor()
for row in cmpls:
    cid = row[0]
    for term in term_ex(row[1] + ' ' + row[2]):
        sql = 'EXECUTE dbo.up_NewTerm @term = ?, @tcount = ?, @id_complain = ?'
        args = [str(term.normalized).replace('"', '').replace('\'', ''), term.count , cid]
        cur.execute(sql, args)
    conn.commit()

cur1.close()
cur.close()
conn.close()